<?php
    //global variable used for hosting connection
    $dbusername = '3009948_ecom';
    $dbpassword = '3secret4password';
    $dbserver = 'fdb26.biz.nf';
    $dbname='3009948_ecom';

    //run this on console if u using docker
    //docker run --name bizecomdb  -e MYSQL_ROOT_PASSWORD=3secret4password -e MYSQL_USER=3009948_ecom -e MYSQL_PASSWORD=3secret4password -e MYSQL_DATABASE=3009948_ecom -p 3306:3306 -d mysql --default-authentication-plugin=mysql_native_password

    // hosting db connection
    $conn = mysqli_connect( "$dbserver", "$dbusername", "$dbpassword", "$dbname" );

    if( !$conn ) // == null if creation of connection object failed
    {
        // docker
        $conn = mysqli_connect( "127.0.0.1", "$dbusername", "$dbpassword", "$dbname" )
        // report the error to the user, then exit program
        or die ("connection object not created: ".mysqli_error($conn));
    }

    if( mysqli_connect_errno() )  // returns false if no error occurred
    {
        // report the error to the user, then exit program
        die ("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
    }
